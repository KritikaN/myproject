package steps;

import cucumber.api.java.Before;
import cucumber.api.java.After;

import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.Scenario;

public class Hooks extends SeleniumBase{
@Before
public void beforeScenario(Scenario sc)
{
	System.out.println(sc.getName());
	System.out.println(sc.getId());
	startReport();
	test = extent.createTest(sc.getName(), sc.getId());
    test.assignAuthor("kritika");
    test.assignCategory("smoke");
    startApp("chrome", "http://leaftaps.com/opentaps/control/main");
    
}
@After
public void afterScenario(Scenario sc)
{
	 System.out.println(sc.getStatus());
	 close();
	 stopReport();
	 
}
}
