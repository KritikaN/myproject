package Runner;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;



@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/feature/CreateLeadIntegration.feature"
		         ,glue= {"com.yalla.pages","steps"}
		         ,monochrome=true
		         //,dryRun=true
		         //,snippets=SnippetType.CAMELCASE
		         //,tags ="@reg"
		         //,tags ="@smoke or @reg"
		         )
		
		
public class RunTest {

}
