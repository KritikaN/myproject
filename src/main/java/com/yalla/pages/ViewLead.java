package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Then;

public class ViewLead extends Annotations {
      public ViewLead() {
    	  PageFactory.initElements(driver, this);
      }
      @FindBy(id="viewLead_companyName_sp")WebElement eleverifycname;
      @Then("verify values entered in createLead is success")
      public ViewLead verifyCmpnyName(String expText) {
    	  
    	  verifyPartialText(eleverifycname, expText);
      
		return this;
      }}
      /*}
      public ViewLead verifyFirstName(String expFText) {
    	  WebElement eleVFirstName = locateElement("id", "viewLead_firstName_sp");
    	  verifyPartialText(eleVFirstName, expFText);
		return this;
      }
        
     public ViewLead verifyLastName(String expFText) {
	  WebElement eleVLastName = locateElement("id", "viewLead_firstName_sp");
	  verifyPartialText(eleVLastName, expFText);
	return this;
     }
      }*/
