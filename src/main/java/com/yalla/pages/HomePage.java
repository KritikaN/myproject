package com.yalla.pages;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import cucumber.api.java.en.And;
import com.yalla.testng.api.base.Annotations;

public class HomePage extends Annotations{ 

	public HomePage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	@FindBy(how=How.LINK_TEXT, using="CRM/SFA") WebElement eleCRM_SFA;
	public LoginPage clickLogout() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);  
		return new LoginPage();

	}
	@And("Click on CrmSfa link")
	public MyHomePage clickCRM_SFALink() {
		click(eleCRM_SFA);
		return new MyHomePage();
	}
	

}







