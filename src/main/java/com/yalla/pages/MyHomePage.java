package com.yalla.pages;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import cucumber.api.java.en.And;
import com.yalla.testng.api.base.Annotations;

public class MyHomePage extends Annotations{
	public MyHomePage() {
	       PageFactory.initElements(driver, this);
		} 
	
	@FindBy(how=How.LINK_TEXT, using="Leads") WebElement eleLeads;
	@And("Click on Leads tab")
	public MyLeads clickLeads(){
		//WebElement eleLeads = locateElement("link", "Leads");
		click(eleLeads);  
		return new MyLeads();
	}


}
