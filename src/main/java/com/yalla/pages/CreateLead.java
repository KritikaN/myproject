package com.yalla.pages;

import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import com.yalla.testng.api.base.Annotations;

public class CreateLead extends Annotations{

	public CreateLead()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using="createLeadForm_companyName")  WebElement eleCompanyName;
	@FindBy(how=How.ID, using="createLeadForm_firstName")  WebElement eleFirstName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.CLASS_NAME, using="smallSubmit") WebElement eleCreateLeadButton;
	
	@And("Enter Company name as (.*)")
	public CreateLead enterCompanyName(String data) {
		//WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleCompanyName, data);  
		return this; 
	}
	@And("Enter First name as (.*)")
    public CreateLead enterFirstName(String data) {
		//WebElement elePassWord = locateElement("id", "password");
		clearAndType(eleFirstName, data); 
		return this; 
	}
	@And("Enter Last name as(.*)")
	public CreateLead enterLastName(String data) {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
		 clearAndType(eleLastName, data);  
          return this;
	}
	@When("Click on the Createlead button")
	public ViewLead clickCreateLead() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
          click(eleCreateLeadButton);  
          return new ViewLead();
	}
}
