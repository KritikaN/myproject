package com.yalla.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeads extends Annotations{
	 public FindLeads() {
		 PageFactory.initElements(driver, this);
	 }
	 
	    @FindBy(how=How.XPATH, using="(//input[@name='firstName']) [3]")  WebElement eleFirstName;
		@FindBy(how=How.XPATH, using="(//td[@class='x-panel-btn-td']//button)[6]") WebElement eleFindLeadsButton;
		
		public FindLeads enterUserName(String data) {
			//WebElement eleUserName = locateElement("id", "username");
			clearAndType(eleFirstName, data);  
			return this; 
		}
 
		
		public FindLeads clickFindLeads() {
			//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	          click(eleFindLeadsButton);  
	          return this;
		}
		
		//public ViewLead getFirstName() {
			
		//}
}
