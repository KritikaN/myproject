package com.yalla.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TC_003_AcmeTest{

     @Test
	public void acmeTest() {
	 System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
     ChromeDriver driver=new ChromeDriver();
     driver.get(" https://acme-test.uipath.com/vendors/results-by-name/)");
     driver.findElementById("email").sendKeys("kritsvirranarware18@gmail.com");
     driver.findElementById("password").sendKeys("12345");
     driver.findElementById("buttonLogin").click();
     driver.manage().window().maximize();
	 driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	 driver.getKeyboard().sendKeys(Keys.ESCAPE);
	 Actions builder= new Actions(driver);
	 WebElement eleVendors = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
	 builder.moveToElement(eleVendors).perform();
	 WebDriverWait wait=new WebDriverWait(driver,20); 
	 WebElement eleSearchVendor=driver.findElementByLinkText("Search for Vendor");
	 wait.until(ExpectedConditions.elementToBeClickable(eleSearchVendor));
	 eleSearchVendor.click();
	 driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
	 driver.findElementById("vendorTaxID").sendKeys("DE767565");
	 driver.findElementById("buttonSearch").click();
	 String text = driver.findElementByXPath("//table[@class='table']//tr[2]//td[1]").getText();
	 System.out.println(text);
	 driver.close();
}
}