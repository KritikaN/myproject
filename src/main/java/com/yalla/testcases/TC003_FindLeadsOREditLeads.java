package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;


public class TC003_FindLeadsOREditLeads extends Annotations {

	@BeforeTest
	public void setData() {
		testcaseName = "TC003_FindLeadsOREditLeads";
		testcaseDec = "Editing leaftaps";
		author = "Kritika";
		category = "smoke";
		excelFileName = "TC003";
	} 
	@Test(dataProvider="fetchData") 
      public void findAndEdit(String uName, String pwd ,String cName,String fName,String lName,String uNAME) {
      {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
	    .clickCRM_SFALink()
	    .clickLeads();
		
	    
      }
	
	}
}
