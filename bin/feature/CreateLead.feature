Feature: Login for leaftaps application 

Background: 
	Given open the browser 
	And Maximize the browser 
	And load the url 
@reg 
Scenario Outline: Positive Login flow 
	And Enter the username as <username> 
	And Enter the password as <password> 
	When Click on the login button 
	Then verify login is success 
	And Click on Crm/Sfa link 
	And Click on Leads tab 
	And Click on CreateLead 
	And Enter Company name as <CompanyName> 
	And Enter First name as <FirstName> 
	And Enter Last name as <LastName> 
	When Click on the Createlead button 
	Then verify values entered in createLead is success 
	
	Examples: 
		|username|password|CompanyName|FirstName|LastName|
		|DemoSalesmanager|crmsfa|CTS|Kritika|N|
		
		
		
		Scenario: Negative Login flow 
			And Enter the username as DemoSalesManager 
			And Enter the password as crmsfa 
			When Click on the login button 
			Then verify login is success 
			And Click on Crm/Sfa link 
			And Click on Leads tab 
			And Click on CreateLead 
			And Enter Company name as TCS
			And Enter First name as Gopi
			And Enter Last name as JJJJ
			When Click on the Createlead button 
			Then verify values entered in createLead is success